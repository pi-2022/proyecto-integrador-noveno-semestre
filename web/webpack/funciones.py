

def validarClave(contraseña):
    mayusculas = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    minusculas = "abcdefghijklmnñopqrstuvwxyz"
    numeros = "0123456789"
    
    caracteresEspeciales = ["~", "@", "#", "_", "^", "-", "*", "%", "/", ".", "+", ":", ";", " "
                             "=", "$", "&", "?", "¿¡", "!", "ª", "<", ">", ",", "(", ")", "º" ]
    resultado = []
    countmay = 0
    countmin = 0
    countnum = 0
    countcar = 0
    countlar = 0

    contra = contraseña
    
    for c in contra:
        
        for m in mayusculas:
            if c == m:
                countmay += 1
    
    for c in contra:
        
        for m in minusculas:
            if c == m:
                countmin += 1
    
    for c in contra:
        
        for n in numeros:
            if c == n:
                countnum += 1
    
    for c in contra:
        
        for car in caracteresEspeciales:
            if c == car:
                countcar += 1
    
    for c in contra:
        countlar += 1

    if countlar < 0:
        resultado.append('La contraseña es demasiado corta')
    
    if len(resultado) == 0:

        if countmay < 1:
            resultado.append('No existen mayusculas en la contraseña')
        
        if countmin < 1:
            resultado.append('No existen minuscula en la contraseña')
        
        if countnum < 1:
            resultado.append('No existen numeros en la contraseña')
    
        if countcar < 1:
            resultado.append('No existen caracteres especiales en la contraseña')
    


    return resultado
 


def camposvacios(**kwargs):
    user = kwargs
    mensaje= []

    for i, valor in user.items():
        if valor == '':
            mensaje.append(f'No ha ingresado: {i}')
        

    return mensaje




