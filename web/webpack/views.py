import sqlite3, hashlib
from flask import render_template, request, redirect, url_for, flash
from webpack import app
from webpack.models import db, Registro, Form, key, Antiguas
from tkinter import messagebox as me
from webpack.funciones import validarClave, camposvacios
import requests, json


usuario = ('*args')
response_txt = ''

#Pantalla principal
@app.route('/')
@app.route('/home')
def home():   
    db.create_all()
    key()
    return render_template('index.html')


#Otras rutas de navegacion
@app.route('/Iniciosesion')
def inicio():
    key()
    return render_template('iniciarSesion.html')


@app.route('/validarinicio', methods=['POST'])

def validar_registro():
    key() 
    conexion = sqlite3.connect('auditoria.db')
    cursor = conexion.cursor()
    user=request.form['usuarioSesion']
    cursor.execute(f"SELECT * FROM registro WHERE user IS '{user}'")
    
    global usuario
    usuario = cursor.fetchone()

    conexion.close()
    validacion = ''

    
    Contingresada = request.form['contraseñaSesion']
    aux = Contingresada.encode('ASCII')
    aux2 = hashlib.new("sha512", aux)
    encrypted = aux2.hexdigest()

    if (usuario == None):
        flash('El usuario ingresado es incorrecto')
        
    elif(request.form['usuarioSesion'] == usuario[4] and encrypted == usuario[6]):
        
        
        #me.showinfo(title='hola', message='ERROR')
        
        return redirect(url_for('form'))
    else:
        flash('La contraseña ingresada es incorrecta ')
    
    
    mes =camposvacios( usuario = request.form['usuarioSesion'], contraseña = request.form['contraseñaSesion'] )
    if(len(mes) == 2):
        flash(mes[0])
        flash(mes[1])
    elif(len(mes) == 1):
        flash(mes[0])
    else:
        pass
    

    
    
    return redirect(url_for('inicio'))
        

@app.route('/registrar', methods=['POST'])
def registrar():
    key()
    '''
    def verificar_captcha(response_captcha):
        secret = "6Le1gNQiAAAAAK5opqU9nrVgIpqPSjNzHNoqk_rW"
        payload = {'response': response_captcha,'secret':secret }
        response = requests.post("https://www.google.com/recaptcha/api/siteverify", payload)
        response_txt = json.loads(response.text)   
        return response_txt['success']
    if request.method == 'POST':
        print('si')
        #captcha = request.form['g-recaptcha-response']
        captcha = ''

    print (captcha)
    a = verificar_captcha(captcha)
    
    print(a)
    
    b = []
    for i in a:
        b.append(i[1])
    print(b)'''
    '''if b[0] == False:
        flash("Captcha no verificado")
    else:
        flash("Captcha verificado")
    '''
    
    #Obtener usuario por su clave primaria de la DB
    conexion = sqlite3.connect('auditoria.db')
    cursor = conexion.cursor()
    cedula=request.form['numeroDocumento']
    cursor.execute(f"SELECT * FROM registro WHERE num_docu IS '{cedula}'")
    global usuario
    usuario = cursor.fetchone()
    conexion.close()

    #validacion si existen campos vacios
    campos = camposvacios(
            numero_documento = request.form['numeroDocumento'],
            nombre = request.form['nomb'],
            apellido = request.form['apellidos'],
            usuario = request.form['IngUsuario'],
            correo = request.form['correo'],
            contraseña = request.form['IngContraseña'])

    contraval = validarClave(request.form['IngContraseña'])
    mail = request.form['correo']
    for con in contraval:
        flash(con)
    
    
    if usuario == None:
        
        if len(campos) > 0:
            for c in campos:
                flash(c)
        else:

            a= request.form['numeroDocumento']
            try: 
                a =int(a)
            except:
                a = a
            
            if '@gmail.com' == mail[-10:] or '@hotmail.com' == mail[-12:] or '@outlook.com' == mail[-12:]:

                if type(a) != int:
                    if len(a) < 8:
                        flash('La cedula debe tener almenos 8 digitos')
                    else:
                        flash('El numero de documento no es numerico')
                    
                else:
                    
                    if request.form['IngContraseña'] != request.form['confContraseña']: 
                        flash('Las contraseñas no coincides')    
                    else:

                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        userdb=request.form['IngUsuario']
                        cursor.execute(f"SELECT * FROM registro WHERE user IS '{userdb}'")
                        userdb = cursor.fetchone()
                        conexion.close()




                        if userdb != None:
                            flash('El nombre de usuario ya existe en la base de datos')
                        else:
                            auxcont = request.form['IngContraseña']
                            aux2 = auxcont.encode('ASCII')
                            encriptada = hashlib.new("sha512", aux2)
                            conthexa = encriptada.hexdigest()
                            regis = Registro(
                                num_docu = request.form['numeroDocumento'],
                                nombre = request.form['nomb'],
                                apellido = request.form['apellidos'],
                                tip_docu = request.form['cedula'],
                                user = request.form['IngUsuario'],
                                correo = request.form['correo'],
                                contraseña = conthexa,
                                rol = 'User')
                            
                            db.session.add(regis)
                            db.session.commit()
                            flash('SAVED')
                    
            else:
                flash('El correo no contiene un dominio real')
            
    else:
            flash('Ya existe un usuario con ese numero de documento')
    

    
    return redirect(url_for('registro'))
@app.route('/Form')
def form():
    key()
    global usuario
    nombre = usuario[4]
    
    return render_template('pagFormulario.html', nom = nombre)

@app.route('/Formres', methods=['POST'])
def obtenerform():
    key()
    p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16 = 'No','No','No','No','No','No','No','No','No','No','No','No','No','No','No','No'
    obtener = Form(p1=p1,p2=p2,p3=p3,p4=p4,p5=p5,p6=p6,p7=p7,p8=p8,p9=p9,p10=p10,p11=p11,p12=p12,p13=p13,p14=p14,p15=p15,p16=p16)
    db.session.add(obtener)
    db.session.commit()
    
    
    return ''#redirect(url_for('resultados'))

@app.route('/Resultados', methods=['POST'])
def resultados():

    return render_template('Resultados.html')

@app.route('/Admin')
def admin():
    key()
    global usuario
    usuarioa = usuario
    nombre = usuarioa[1]
    apellido = usuarioa[2]
    email = usuarioa[5]
    tipodoc = usuarioa[3]
    numdoc = usuarioa[0]
    conexion = sqlite3.connect('auditoria.db')
    cursor = conexion.cursor()
    cursor.execute(f"SELECT user FROM registro")
    
    registros = cursor.fetchall()
    conexion.close()
    a = []
    for i in registros:
        a.append(i[0])
    return render_template('perfilAdmin.html', nombre=nombre, apellido=apellido, email=email, tipodoc=tipodoc, numdoc=numdoc, lista = a)

@app.route('/Gestionrol', methods=['POST'])
def gestion_rol():
    key()
    nombre = request.form.to_dict('Dios')
    o = nombre.get('Dios', 'No se encontro')
    w = nombre.get('Luci', 'No se encontro')
    print(o)
    conexion = sqlite3.connect('auditoria.db')
    cursor = conexion.cursor()
    cursor.execute(f"SELECT * FROM registro WHERE user IS '{o}'")
    conexion.commit()
    rego = cursor.fetchone()
    cursor.close()
    '''asj = []
    print(type(asj))'''
    
    print(rego)
    print(rego[0])
    print(w)
    '''for i in reg:
        asj.append(i[0])'''
    f = rego[0]
    mensaje = f"UPDATE registro SET rol = '{w}' WHERE num_docu IS {rego[0]}"
    print(mensaje)
    if rego[7] == w:
        flash('El usuario ya tiene asignado este rol')
    else:
        conexion = sqlite3.connect('auditoria.db')
        cursor = conexion.cursor()
        cursor.execute(mensaje)
        conexion.commit()
        cursor.close()
        flash('El rol fue cambiado satisfactoriamente')
    return redirect(url_for('tipo_usuario'))

@app.route('/Select')
def tipo_usuario():
    key()
    global usuario
    if usuario[7] == 'User':
        return redirect(url_for('usuario'))
    elif usuario[7] == 'Admin':
        return redirect(url_for('admin'))

@app.route('/Usuario')
def usuario():
    key()
    global usuario
    usuariog = usuario
    nombre = usuariog[1]
    apellido = usuariog[2]
    email = usuariog[5]
    tipodoc = usuariog[3]
    numdoc = usuariog[0]
    #(3421, 'joel', 'castro', 'Cedula', 'bryroa', 'bry@gmail', '555', 'User')
    return render_template('perfilusuario.html', nombre=nombre, apellido=apellido, email=email, tipodoc=tipodoc, numdoc=numdoc)

@app.route('/Registro')
def registro():
    key()
    return render_template('registrarse.html')

@app.route('/Actualizar', methods=['POST'])
def cambiar_contraseña():
    key()
    global usuario

    #Obtener usuario por su clave primaria de la DB
    conexion = sqlite3.connect('auditoria.db')
    cursor = conexion.cursor()
    cursor.execute(f"SELECT * FROM Antiguas WHERE num_docu IS '{usuario[0]}'")
    ContAntiguas = cursor.fetchone()
    conexion.close()

    campos = camposvacios(
            Contraseña_actual = request.form['nuevaContraseña'],
            Nueva_contraseña = request.form['actualContraseña']
            )

    contraval = validarClave(request.form['actualContraseña'])

    Contnueva = request.form['actualContraseña']
    aux = Contnueva.encode('ASCII')
    aux2 = hashlib.new("sha512", aux)
    encrypted = aux2.hexdigest()

    Contactual = request.form['nuevaContraseña']
    auxa = Contactual.encode('ASCII')
    auxa2 = hashlib.new("sha512", auxa)
    encrypted2 = auxa2.hexdigest()

    if encrypted2 == usuario[6]:
        
        if len(campos) == 0:
            if len(contraval) == 0:

                
                if ContAntiguas != None:
                    v1 = ContAntiguas[2]
                    v2 = ContAntiguas[3]
                    v3 = ContAntiguas[4]

                    if v1 != encrypted and v2 != encrypted and v3 != encrypted:
                    
                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        nueva_contraseña=encrypted
                        cursor.execute(f"UPDATE registro SET contraseña = '{nueva_contraseña}' WHERE num_docu IS {usuario[0]}")
                        conexion.commit()
                        conexion.close()
                        flash('Se ha actualizado correctamente')
                        
                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        cursor.execute(f"SELECT * FROM registro WHERE num_docu IS '{usuario[0]}'")
                        usuario = cursor.fetchone()



                        if v2 == '':
                            v2 = v1
                            v1 = encrypted2

                            
                        else:
                            v3 = v2
                            v2 = v1
                            v1 = encrypted2
                        
                            
                        #reescribiendo los valores de usuario global
                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        cursor.execute(f"UPDATE Antiguas SET ContAnt1 = '{v1}', ContAnt2 = '{v2}', ContAnt3 = '{v3}' WHERE num_docu IS {usuario[0]}")
                        conexion.commit()
                        conexion.close()
                    else:
                        flash('Esta contraseña ha sido usada anteriormente, NO SE PUEDO REALIZAR EL CAMBIO')

                else:
                        aux = Antiguas(
                                        num_docu = usuario[0],
                                        user = usuario[4],
                                        ContAnt1 = encrypted2,
                                        ContAnt2 = '',
                                        ContAnt3 = '')                       
                        db.session.add(aux)
                        db.session.commit()
                        db.session.close()

                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        nueva_contraseña=encrypted
                        cursor.execute(f"UPDATE registro SET contraseña = '{nueva_contraseña}' WHERE num_docu IS {usuario[0]}")
                        conexion.commit()
                        conexion.close()
                        flash('Se ha actualizado correctamente')

                        #reescribiendo los valores de usuario global
                        conexion = sqlite3.connect('auditoria.db')
                        cursor = conexion.cursor()
                        cursor.execute(f"SELECT * FROM registro WHERE num_docu IS '{usuario[0]}'")
                        usuario = cursor.fetchone()

                



            else:
                for con in contraval:
                    flash(con)
            
        else:
            for c in campos:
                flash(c)      

    else:
        flash('La contraseña es igual a la actual')    
    
     
    return redirect(url_for('tipo_usuario'))
