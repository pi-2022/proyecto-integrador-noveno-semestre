from webpack import app
from flask_sqlalchemy import SQLAlchemy

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../auditoria.db'
def key():
    app.config['SECRET_KEY'] = '123456ABCDEF'

db = SQLAlchemy(app)


class Registro(db.Model):
    num_docu = db.Column(db.Integer(), primary_key=True)
    nombre = db.Column(db.String(30))
    apellido = db.Column(db.String(30))
    tip_docu = db.Column(db.String(20))
    user = db.Column(db.String(15))
    correo = db.Column(db.String(30))
    contraseña = db.Column(db.String(500))
    rol = db.Column(db.String(10))

class Form(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    p1 = db.Column(db.String())
    p2 = db.Column(db.String())
    p3 = db.Column(db.String())
    p4 = db.Column(db.String())
    p5 = db.Column(db.String())
    p6 = db.Column(db.String())
    p7 = db.Column(db.String())
    p8 = db.Column(db.String())
    p9 = db.Column(db.String())
    p10 = db.Column(db.String())
    p11 = db.Column(db.String())
    p12 = db.Column(db.String())
    p13 = db.Column(db.String())
    p14 = db.Column(db.String())
    p15 = db.Column(db.String())
    p16 = db.Column(db.String())

class Antiguas(db.Model):
    num_docu = db.Column(db.Integer(), primary_key=True)
    user = db.Column(db.String(15))
    ContAnt1 = db.Column(db.String(500))
    ContAnt2 = db.Column(db.String(500))
    ContAnt3 = db.Column(db.String(500))
