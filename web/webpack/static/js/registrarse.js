let formulario = document.forms['formularioRegistro'];
let nombre = formulario['nomb'];
let apellido = formulario['apellidos'];
let numdoc = formulario['numeroDocumento'];
let usuario = formulario['IngUsuario'];
let correo = formulario['correo'];
let contra = formulario['IngContraseña'];
let confContra = formulario['confContraseña'];

let mayusculas = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
let minusculas = "abcdefghijklmnñopqrstuvwxyz";
let numeros = "0123456789";
let caracteresEspeciales = "~@#_^-*%/.+:;=$&?¿¡!ª<>,()º";

function crearcuenta() {

    if (nombre.value == "" && apellido.value == "" && numdoc.value == "" && usuario.value == "" && correo.value == "" && contra.value == "" && confContra.value == "") {
        alert('ERROR: MUCHOS CAMPOS VACIOS');
    } else if (nombre.value == "") {
        alert('ERROR: FALTA NOMBRE');
    } else if (apellido.value == "") {
        alert('ERROR: FALTA APELLIDO');
    } else if (numdoc.value == "") {
        alert('ERROR: FALTA NUMERO DE DOCUMENTO');
    } else if (usuario.value == "") {
        alert('ERROR: FALTA USUARIO');
    } else if (correo.value == "") {
        alert('ERROR: FALTA CORREO ELECTRONICO');
    } else if (contra.value == "") {
        alert('ERROR: FALTA CONTRASEÑA ');
    } else if (confContra.value == "") {
        alert('ERROR: FALTA CONFIRMAR CONTRASEÑA');
    }


}

function mostrarbarra() {


    document.getElementById('mostrarbarra').style.display = 'inline';

    /* esta variable convierte lacontraseña ingresada en un string */
    let guardarContra = String(contra.value);




    let contarMayus = 0;
    let contarMinus = 0;
    let contarNums = 0;
    let contarEspeciales = 0;

    /* for para contar mayusculas */
    for (var i = 0; i < mayusculas.length; i++) {
        for (var x = 0; x < guardarContra.length; x++) {
            if (guardarContra[x] == mayusculas[i]) {
                contarMayus += 1;
            }
        }
    }

    /* for para contar minusculas */
    for (var i = 0; i < minusculas.length; i++) {
        for (var x = 0; x < guardarContra.length; x++) {
            if (guardarContra[x] == minusculas[i]) {
                contarMinus += 1;
            }
        }
    }

    /* for para contar numeros en la contraseña */
    for (var i = 0; i < numeros.length; i++) {
        for (var x = 0; x < guardarContra.length; x++) {
            if (guardarContra[x] == numeros[i]) {
                contarNums += 1;
            }
        }
    }

    /* for para contar caracteres especiales */
    for (var i = 0; i < caracteresEspeciales.length; i++) {
        for (var x = 0; x < guardarContra.length; x++) {
            if (guardarContra[x] == caracteresEspeciales[i]) {
                contarEspeciales += 1;
            }
        }
    }



    /* ------------------------------------------SE REVISAN CUANTAS CONDICIONES SE CUMPLIERON------------------------
 */



    let resultados = [contarMayus, contarMinus, contarNums, contarEspeciales];

    /* ESTA VARIABLE NOS PUEDE AYUDAR A NO GUARDAR NADA EN LA BASE DE DATOS HASTA QUE EL USUARIO NO CUMPLA CON LAS 5 DE 5 CONDICIONES */
    let contarCondicionesCumplidas = 0;

    if (guardarContra.length >= 12) {
        contarCondicionesCumplidas++;
    }

    for (let index = 0; index < resultados.length; index++) {

        if (resultados[index] != 0) {
            contarCondicionesCumplidas++;
        }


    }




    if (contarCondicionesCumplidas == 0 || contarCondicionesCumplidas == 1 || contarCondicionesCumplidas == 2) {

        document.getElementById('modificarbarra').style.width = '25%';
        document.getElementById('modificarbarra').innerHTML = 'INSEGURA';

    }
    else if (contarCondicionesCumplidas == 3) {

        document.getElementById('modificarbarra').style.width = '50%';
        document.getElementById('modificarbarra').innerHTML = 'REGULAR';
    } else if (contarCondicionesCumplidas == 4) {

        document.getElementById('modificarbarra').style.width = '75%';
        document.getElementById('modificarbarra').innerHTML = 'BUENA';
    } else if (contarCondicionesCumplidas == 5) {

        document.getElementById('modificarbarra').style.width = '100%';
        document.getElementById('modificarbarra').innerHTML = 'EXCELENTE';
    }


    /* -----------------------cambiar logos de requisitos ------------------------*/


    /* revisa si la contraseña es mayor de 12 */
    if (guardarContra.length >= 12) {
        document.getElementById('requ12Caracteres').src = "/static/img/registro/check ok.png";
    } else {
        document.getElementById('requ12Caracteres').src = "/static/img/registro/check no.png";
    }


    /* revisa si hay mayuscula almenos 1 */
    if (contarMayus != 0) {
        document.getElementById('reqMayusculas').src = "/static/img/registro/check ok.png";
    } else {
        document.getElementById('reqMayusculas').src = "/static/img/registro/check no.png";
    }

    /* revisa si hay minuscula almenos 1 */
    if (contarMinus != 0) {
        document.getElementById('reqMinusculas').src = "/static/img/registro/check ok.png";
    } else {
        document.getElementById('reqMinusculas').src = "/static/img/registro/check no.png";
    }


    /* revisa si hay números almenos 1 */
    if (contarNums != 0) {
        document.getElementById('reqNumero').src = "/static/img/registro/check ok.png";
    } else {
        document.getElementById('reqNumero').src = "/static/img/registro/check no.png";
    }

    /* revisa si hay caracter especial almenos 1 */
    if (contarEspeciales != 0) {
        document.getElementById('reqCarEsp').src = "/static/img/registro/check ok.png";
    } else {
        document.getElementById('reqCarEsp').src = "/static/img/registro/check no.png";
    }



}


function mostrarContraseña() {
    var tipo = document.getElementById('pass');
    if (tipo.type == "password") {
        tipo.type = "text";
        document.getElementById('imgVerContra').src = '/static/img/registro/no ver.png';
        document.getElementById('verContra').style.backgroundColor = 'black';
    } else {
        tipo.type = "password";
        document.getElementById('imgVerContra').src = '/static/img/registro/ver.png';
        document.getElementById('verContra').style.backgroundColor = '#6C757D';
    }
}
function mostrarConfContraseña() {
    var tipo = document.getElementById('Confpass');
    if (tipo.type == "password") {
        tipo.type = "text";
        document.getElementById('imgVerConfContra').src = '/static/img/registro/no ver.png';
        document.getElementById('verConfContra').style.backgroundColor = 'black';
    } else {
        tipo.type = "password";
        document.getElementById('imgVerConfContra').src = '/static/img/registro/ver.png';
        document.getElementById('verConfContra').style.backgroundColor = '#6C757D';
    }
}


function habilitar(response) {
    
    document.getElementsByName('crearcuenta')[0].disabled=false;

    
    
}